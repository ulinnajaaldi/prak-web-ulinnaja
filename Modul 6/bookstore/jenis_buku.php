<?php

include 'koneksi.php';

function tambah_jenis_buku($kode_jenis, $nama_jenis, $keterangan_jenis)
{
    global $conn;

    $kode_jenis = mysqli_real_escape_string($conn, $kode_jenis);
    $nama_jenis = mysqli_real_escape_string($conn, $nama_jenis);
    $keterangan_jenis = mysqli_real_escape_string($conn, $keterangan_jenis);

    // Query untuk menambahkan data jenis_buku
    $sql = "INSERT INTO jenis_buku (kode_jenis, nama_jenis, keterangan_jenis) VALUES ('$kode_jenis', '$nama_jenis', '$keterangan_jenis')";

    if (mysqli_query($conn, $sql)) {
        return true;
    } else {
        return false;
    }
}

function ubah_jenis_buku($kode_jenis, $nama_jenis, $keterangan_jenis)
{
    global $conn;

    $kode_jenis = mysqli_real_escape_string($conn, $kode_jenis);
    $nama_jenis = mysqli_real_escape_string($conn, $nama_jenis);
    $keterangan_jenis = mysqli_real_escape_string($conn, $keterangan_jenis);

    // Query untuk mengubah data jenis_buku
    $sql = "UPDATE jenis_buku SET nama_jenis='$nama_jenis', keterangan_jenis='$keterangan_jenis' WHERE kode_jenis='$kode_jenis'";

    if (mysqli_query($conn, $sql)) {
        return true;
    } else {
        return false;
    }
}

function hapus_jenis_buku($kode_jenis)
{
    global $conn;
    $kode_jenis = mysqli_real_escape_string($conn, $kode_jenis);

    // Query untuk menghapus data jenis_buku
    $sql = "DELETE FROM jenis_buku WHERE kode_jenis='$kode_jenis'";

    if (mysqli_query($conn, $sql)) {
        return true;
    } else {
        return false;
    }
}

function get_jenis_buku($kode_jenis = null)
{
    global $conn;

    // Query untuk mengambil semua data jenis_buku
    $sql = "SELECT * FROM jenis_buku";

    // Jika kode_jenis tidak null, tambahkan WHERE clause untuk memfilter berdasarkan kode_jenis
    if ($kode_jenis != null) {
        $kode_jenis = mysqli_real_escape_string($conn, $kode_jenis);
        $sql .= " WHERE kode_jenis='$kode_jenis'";
    }

    $result = mysqli_query($conn, $sql);

    // Konversi hasil query ke array asosiatif
    $data = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $data[] = $row;
    }

    return $data;
}
