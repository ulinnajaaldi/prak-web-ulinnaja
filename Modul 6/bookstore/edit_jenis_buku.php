<?php
include 'koneksi.php';
include 'jenis_buku.php';

// Fungsi untuk mendapatkan data jenis_buku berdasarkan kode_jenis
function get_jenis_buku_by_kode($kode_jenis)
{
    global $conn;
    $sql = "SELECT * FROM jenis_buku WHERE kode_jenis='$kode_jenis'";
    $result = mysqli_query($conn, $sql);

    // Mengubah hasil query menjadi array asosiatif
    $data = mysqli_fetch_assoc($result);
    return $data;
}

// Jika tombol update diklik
if (isset($_POST['update'])) {
    $kode_jenis = $_POST['kode_jenis'];
    $nama_jenis = $_POST['nama_jenis'];
    $keterangan_jenis = $_POST['keterangan_jenis'];

    if (ubah_jenis_buku($kode_jenis, $nama_jenis, $keterangan_jenis)) {
        header('Location: index.php');
    } else {
        $error_message = 'Gagal mengubah data jenis_buku';
    }
}

// Mendapatkan data jenis_buku berdasarkan kode_jenis yang diberikan
if (isset($_GET['kode_jenis'])) {
    $kode_jenis = $_GET['kode_jenis'];
    $data_jenis_buku = get_jenis_buku_by_kode($kode_jenis);
} else {
    header('Location: jenis_buku.php');
}
?>
<link rel="stylesheet" href="style/style.css">
<div class="container">
    <h2>Edit Jenis Buku</h2>

    <?php if (isset($error_message)) : ?>
        <div class="alert alert-danger"><?php echo $error_message; ?></div>
    <?php endif; ?>

    <form method="post">
        <div class="form-group">
            <label for="kode_jenis">Kode Jenis Buku</label>
            <input type="text" class="form-control" id="kode_jenis" name="kode_jenis" value="<?php echo $data_jenis_buku['kode_jenis']; ?>" readonly>
        </div>
        <div class="form-group">
            <label for="nama_jenis">Nama Jenis Buku</label>
            <input type="text" class="form-control" id="nama_jenis" name="nama_jenis" value="<?php echo $data_jenis_buku['nama_jenis']; ?>">
        </div>
        <div class="form-group">
            <label for="keterangan_jenis">Keterangan Jenis</label>
            <input type="text" class="form-control" id="keterangan_jenis" name="keterangan_jenis" value="<?php echo $data_jenis_buku['keterangan_jenis']; ?>">
        </div>
        <button type="submit" class="btn btn-primary" name="update">Update</button>
    </form>
</div>