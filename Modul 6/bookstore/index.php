<?php
// Include file koneksi.php
include 'koneksi.php';
include 'jenis_buku.php';
include 'buku.php';

// Proses tambah data jenis_buku
if (isset($_POST['tambah_jenis_buku'])) {
    $kode_jenis = $_POST['kode_jenis'];
    $nama_jenis = $_POST['nama_jenis'];
    $keterangan_jenis = $_POST['keterangan_jenis'];

    tambah_jenis_buku($kode_jenis, $nama_jenis, $keterangan_jenis);

    header('Location: index.php');
}

// Proses ubah data jenis_buku
if (isset($_POST['ubah_jenis_buku'])) {
    $kode_jenis = $_POST['kode_jenis'];
    $nama_jenis = $_POST['nama_jenis'];
    $keterangan_jenis = $_POST['keterangan_jenis'];

    ubah_jenis_buku($kode_jenis, $nama_jenis, $keterangan_jenis);

    header('Location: index.php');
}

// Proses hapus data jenis_buku
if (isset($_GET['hapus_jenis_buku'])) {
    $kode_jenis = $_GET['hapus_jenis_buku'];

    hapus_jenis_buku($kode_jenis);

    header('Location: index.php');
}

// Proses tambah data buku
if (isset($_POST['tambah_buku'])) {
    $kode_buku = $_POST['kode_buku'];
    $nama_buku = $_POST['nama_buku'];
    $kode_jenis = $_POST['kode_jenis'];

    tambah_buku($kode_buku, $nama_buku, $kode_jenis);

    header('Location: index.php');
}

// Proses ubah data buku
if (isset($_POST['ubah_buku'])) {
    $kode_buku = $_POST['kode_buku'];
    $nama_buku = $_POST['nama_buku'];
    $kode_jenis = $_POST['kode_jenis'];

    ubah_buku($kode_buku, $nama_buku, $kode_jenis);

    header('Location: index.php');
}

// Proses hapus data buku
if (isset($_GET['hapus_buku'])) {
    $kode_buku = $_GET['hapus_buku'];

    hapus_buku($kode_buku);

    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Aplikasi Bookstore</title>
    <link rel="stylesheet" href="style/style.css">
</head>

<body>
    <h1 style="text-align: center;">Aplikasi Bookstore</h1>
    <p style="text-align: center; font-size: 10px; margin-top: -20px;">L200200090/ Aldilla Ulinnaja</p>

    <section>
        <div>
            <h2>Tambah Data Jenis Buku</h2>

            <form method="POST" action="index.php">
                <div>
                    <label>Kode Jenis Buku</label>
                    <input type="text" name="kode_jenis">
                </div>
                <div>
                    <label>Nama Jenis Buku</label>
                    <input type="text" name="nama_jenis">
                </div>
                <div>
                    <label>Keterangan Jenis</label>
                    <input type="text" name="keterangan_jenis">
                </div>
                <button type="submit" name="tambah_jenis_buku">Tambah Jenis Buku</button>
            </form>
            <hr style="margin-top: 50px;" />
            <h2>Data Jenis Buku</h2>
            <table border="1">
                <tr>
                    <th>Kode Jenis Buku</th>
                    <th>Nama Jenis Buku</th>
                    <th>Keterangan Jenis</th>
                    <th>Aksi</th>
                </tr>
                <?php
                // Mendapatkan data jenis_buku
                $data_jenis_buku = get_jenis_buku();

                foreach ($data_jenis_buku as $jenis_buku) {
                    echo '<tr>';
                    echo '<td>' . $jenis_buku['kode_jenis'] . '</td>';
                    echo '<td>' . $jenis_buku['nama_jenis'] . '</td>';
                    echo '<td>' . $jenis_buku['keterangan_jenis'] . '</td>';
                    echo '<td><a href="edit_jenis_buku.php?kode_jenis=' . $jenis_buku['kode_jenis'] . '">Edit</a> | <a href="index.php?hapus_jenis_buku=' . $jenis_buku['kode_jenis'] . '">Hapus</a></td>';
                    echo '</tr>';
                }
                ?>
            </table>
        </div>

        <div>
            <h2>Tambah Data buku</h2>
            <form method="POST" action="index.php">
                <div>
                    <label>Kode buku</label>
                    <input type="text" name="kode_buku">
                </div>
                <div>
                    <label>Nama buku</label>
                    <input type="text" name="nama_buku">
                </div>
                <div>
                    <label>Kode Jenis Buku</label>
                    <select name="kode_jenis">
                        <?php
                        // Mendapatkan data jenis_buku
                        $data_jenis_buku = get_jenis_buku();

                        foreach ($data_jenis_buku as $jenis_buku) {
                            echo '<option value="' . $jenis_buku['kode_jenis'] . '">' . $jenis_buku['nama_jenis'] . ' (' . $jenis_buku['keterangan_jenis'] . ')</option>';
                        }
                        ?>
                    </select>
                </div>

                <button type="submit" name="tambah_buku">Tambah buku</button>
            </form>
            <hr style="margin-top: 50px;" />
            <h2>Data buku</h2>
            <table border="1">
                <tr>
                    <th>Kode buku</th>
                    <th>Nama buku</th>
                    <th>Kode Jenis Buku</th>
                    <th>Nama Jenis Buku</th>
                    <th>Aksi</th>
                </tr>
                <?php
                // Mendapatkan data buku
                $data_buku = get_buku();

                foreach ($data_buku as $buku) {
                    echo '<tr>';
                    echo '<td>' . $buku['kode_buku'] . '</td>';
                    echo '<td>' . $buku['nama_buku'] . '</td>';
                    echo '<td>' . $buku['kode_jenis'] . '</td>';
                    echo '<td>' . $buku['nama_jenis'] . '</td>';
                    echo '<td><a href="edit_buku.php?kode_buku=' . $buku['kode_buku'] . '">Edit</a> | <a href="index.php?hapus_buku=' . $buku['kode_buku'] . '">Hapus</a></td>';
                    echo '</tr>';
                }
                ?>
            </table>
        </div>
    </section>
</body>

</html>