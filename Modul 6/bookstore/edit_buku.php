<?php

// Include file koneksi.php
include 'koneksi.php';
include 'jenis_buku.php';

function get_buku_by_kode($kode_buku)
{
    global $conn;
    $sql = "SELECT * FROM buku WHERE kode_buku='$kode_buku'";
    $result = mysqli_query($conn, $sql);

    // Mengubah hasil query menjadi array asosiatif
    $data = mysqli_fetch_assoc($result);
    return $data;
}

function get_all_jenis_buku()
{
    global $conn;
    $sql = "SELECT * FROM jenis_buku";
    $result = mysqli_query($conn, $sql);

    // Membuat array kosong untuk menyimpan data jenis_buku
    $data_jenis_buku = array();

    // Looping untuk mengubah hasil query menjadi array asosiatif dan menyimpannya ke dalam $data_jenis_buku
    while ($jenis_buku = mysqli_fetch_assoc($result)) {
        $data_jenis_buku[] = $jenis_buku;
    }

    return $data_jenis_buku;
}


$kode_buku = $_GET['kode_buku'];

$data_buku = get_buku_by_kode($kode_buku);

if (!$data_buku) {
    die('buku tidak ditemukan');
}

// Jika tombol simpan ditekan
if (isset($_POST['simpan'])) {
    // Mendapatkan data dari form
    $nama_buku = $_POST['nama_buku'];
    $kode_jenis = $_POST['kode_jenis'];

    // Query untuk mengupdate data buku
    $query = "UPDATE buku SET nama_buku='$nama_buku', kode_jenis='$kode_jenis' WHERE kode_buku='$kode_buku'";

    // Menjalankan query
    $result = mysqli_query($conn, $query);

    // Jika query berhasil dijalankan
    if ($result) {
        header('Location: index.php');
    } else {
        echo 'Gagal mengupdate data buku';
    }
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>Edit buku</title>

    <link rel="stylesheet" href="style/style.css">
</head>

<body>
    <h1>Edit buku</h1>

    <form method="POST" action="">
        <div>
            <label>Nama buku</label>
            <input type="text" name="nama_buku" value="<?php echo $data_buku['nama_buku']; ?>">
        </div>
        <div>
            <label>Kode jenis_buku</label>
            <select name="kode_jenis">
                <?php
                // Mendapatkan data jenis_buku

                $data_jenis_buku = get_all_jenis_buku(); // Looping untuk menampilkan opsi pada select
                foreach ($data_jenis_buku as $jenis_buku) {
                    $selected = ($jenis_buku['kode_jenis'] == $data_buku['kode_jenis']) ? 'selected' : '';
                    echo "<option value='" . $jenis_buku['kode_jenis'] . "' $selected>" . $jenis_buku['nama_jenis_buku'] . "</option>";
                }
                ?>
            </select>
        </div>
        <button type="submit" name="simpan">Simpan</button>
    </form>
</body>

</html>