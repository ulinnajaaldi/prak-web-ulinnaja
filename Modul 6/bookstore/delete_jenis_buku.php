<?php

// Include file koneksi.php
include 'koneksi.php';

// Mendapatkan kode jenis_buku dari parameter URL
$kode_jenis = $_GET['kode_jenis'];

// Query untuk menghapus data jenis_buku
$query = "DELETE FROM jenis_buku WHERE kode_jenis='$kode_jenis'";

// Menjalankan query
$result = mysqli_query($koneksi, $query);

// Jika query berhasil dijalankan
if ($result) {
    header('Location: index.php');
} else {
    echo 'Gagal menghapus data jenis_buku';
}
