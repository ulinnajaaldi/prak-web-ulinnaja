<?php

include 'koneksi.php';

function tambah_buku($kode_buku, $nama_buku, $kode_jenis)
{
    global $conn;

    $kode_buku = mysqli_real_escape_string($conn, $kode_buku);
    $nama_buku = mysqli_real_escape_string($conn, $nama_buku);
    $kode_jenis = mysqli_real_escape_string($conn, $kode_jenis);

    // Query untuk menambahkan data
    $sql = "INSERT INTO buku (kode_buku, nama_buku, kode_jenis) VALUES ('$kode_buku', '$nama_buku', '$kode_jenis')";

    if (mysqli_query($conn, $sql)) {
        return true;
    } else {
        return false;
    }
}

function ubah_buku($kode_buku, $nama_buku, $kode_jenis)
{
    global $conn;

    $kode_buku = mysqli_real_escape_string($conn, $kode_buku);
    $nama_buku = mysqli_real_escape_string($conn, $nama_buku);
    $kode_jenis = mysqli_real_escape_string($conn, $kode_jenis);

    // Query untuk mengubah data
    $sql = "UPDATE buku SET nama_buku='$nama_buku', kode_jenis='$kode_jenis' WHERE kode_buku='$kode_buku'";

    if (mysqli_query($conn, $sql)) {
        return true;
    } else {
        return false;
    }
}

function hapus_buku($kode_buku)
{
    global $conn;
    $kode_buku = mysqli_real_escape_string($conn, $kode_buku);

    // Query untuk menghapus data
    $sql = "DELETE FROM buku WHERE kode_buku='$kode_buku'";

    if (mysqli_query($conn, $sql)) {
        return true;
    } else {
        return false;
    }
}

function get_buku($kode_buku = null)
{
    global $conn;
    $sql = "SELECT buku.*, jenis_buku.nama_jenis FROM buku INNER JOIN jenis_buku ON buku.kode_jenis = jenis_buku.kode_jenis";
    if ($kode_buku != null) {
        $kode_buku = mysqli_real_escape_string($conn, $kode_buku);

        // Query untuk mendapatkan data berdasarkan kode_buku
        $sql .= " WHERE kode_buku='$kode_buku'";
    }

    $result = mysqli_query($conn, $sql);

    // Mengubah hasil query menjadi array asosiatif
    $data = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $data[] = $row;
    }
    return $data;
}
