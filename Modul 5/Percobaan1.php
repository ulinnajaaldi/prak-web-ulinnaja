<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Percobaan 1</title>
</head>
<?php
$conn = mysqli_connect("localhost", "root", "", "informatika");
?>

<body>
  <h3>Masukkan Data Mahasiswa :</h3>
  <form method="POST" action='Percobaan1.php'>
    <table border='0' width='30%'>
      <tr>
        <td width="25%">NIM</td>
        <td width="5%">:</td>
        <td width="65%"><input type="text" name="nim" size="10"></td>
      </tr>
      <tr>
        <td width="25%">Nama</td>
        <td width="5%">:</td>
        <td width="65%"><input type="text" name="nama" size="30"></td>
      </tr>
      <tr>
        <td width="25%">Kelas</td>
        <td width="5%">:</td>
        <td width="65%">
          <input type="radio" checked name="kelas" value="A" size="30" />A
          <input type="radio" name="kelas" value="B" size="30" />B
          <input type="radio" name="kelas" value="C" size="30" />C
        </td>
      </tr>
      <tr>
        <td width="25%">Alamat</td>
        <td width="5%">:</td>
        <td width="65%"><input type="text" name="alamat" size="30"></td>
      </tr>
    </table>
    <br />
    <input type="submit" name="submit" value="Simpan">
  </form>
  <?php
  if (isset($_POST['submit'])) {
    $nim = $_POST['nim'];
    $nama = $_POST['nama'];
    $kelas = $_POST['kelas'];
    $alamat = $_POST['alamat'];
    $query = "INSERT INTO mahasiswa (nim, nama, kelas, alamat) VALUES ('$nim', '$nama', '$kelas', '$alamat')";
    if ($nim == '' || $nama == '' || $kelas == '' || $alamat == '') {
      echo "Data belum lengkap";
    } else {
      $result = mysqli_query($conn, $query);
      if ($result) {
        echo "Data berhasil disimpan";
      } else {
        echo "Data gagal disimpan";
      }
    }
  }
  ?>
  <hr>
  <H3>Data Mahasiswa</H3>
  <table border='1'>
    <tr>
      <td align='center'><b>NIM</b></td>
      <td align='center'><b>Nama</b></td>
      <td align='center'><b>Kelas</b></td>
      <td align='center'><b>Alamat</b></td>
      <td align='center'><b>Keterangan</b></td>
    </tr>
    <?php
    $cari = 'SELECT * from mahasiswa order by nim';
    $hasil_cari = mysqli_query($conn, $cari);
    while ($data = mysqli_fetch_row($hasil_cari)) {
      echo "<tr>
      <td>$data[0]</td>
      <td>$data[1]</td>
      <td>$data[2]</td>
      <td>$data[3]</td>
      <td><a href='edit.php?nim=$data[0]'>Edit</a></td>
      </tr>";
    }
    ?>
  </table>
</body>

</html>