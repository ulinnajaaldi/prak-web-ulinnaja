<?php
include 'koneksi.php';
include 'gudang.php';

// Fungsi untuk mendapatkan data gudang berdasarkan kode_gudang
function get_gudang_by_kode($kode_gudang)
{
    global $conn;
    $sql = "SELECT * FROM gudang WHERE kode_gudang='$kode_gudang'";
    $result = mysqli_query($conn, $sql);

    // Mengubah hasil query menjadi array asosiatif
    $data = mysqli_fetch_assoc($result);
    return $data;
}

// Jika tombol update diklik
if (isset($_POST['update'])) {
    $kode_gudang = $_POST['kode_gudang'];
    $nama_gudang = $_POST['nama_gudang'];
    $lokasi = $_POST['lokasi'];

    if (ubah_gudang($kode_gudang, $nama_gudang, $lokasi)) {
        header('Location: index.php');
    } else {
        $error_message = 'Gagal mengubah data gudang';
    }
}

// Mendapatkan data gudang berdasarkan kode_gudang yang diberikan
if (isset($_GET['kode_gudang'])) {
    $kode_gudang = $_GET['kode_gudang'];
    $data_gudang = get_gudang_by_kode($kode_gudang);
} else {
    header('Location: gudang.php');
}
?>

<div class="container">
    <h2>Edit Gudang</h2>

    <?php if (isset($error_message)) : ?>
        <div class="alert alert-danger"><?php echo $error_message; ?></div>
    <?php endif; ?>

    <form method="post">
        <div class="form-group">
            <label for="kode_gudang">Kode Gudang</label>
            <input type="text" class="form-control" id="kode_gudang" name="kode_gudang" value="<?php echo $data_gudang['kode_gudang']; ?>" readonly>
        </div>
        <div class="form-group">
            <label for="nama_gudang">Nama Gudang</label>
            <input type="text" class="form-control" id="nama_gudang" name="nama_gudang" value="<?php echo $data_gudang['nama_gudang']; ?>">
        </div>
        <div class="form-group">
            <label for="lokasi">Lokasi</label>
            <input type="text" class="form-control" id="lokasi" name="lokasi" value="<?php echo $data_gudang['lokasi']; ?>">
        </div>
        <button type="submit" class="btn btn-primary" name="update">Update</button>
    </form>
</div>