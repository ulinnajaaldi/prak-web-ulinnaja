<?php

include 'koneksi.php';

function tambah_barang($kode_barang, $nama_barang, $kode_gudang)
{
    global $conn;

    $kode_barang = mysqli_real_escape_string($conn, $kode_barang);
    $nama_barang = mysqli_real_escape_string($conn, $nama_barang);
    $kode_gudang = mysqli_real_escape_string($conn, $kode_gudang);

    // Query untuk menambahkan data
    $sql = "INSERT INTO barang (kode_barang, nama_barang, kode_gudang) VALUES ('$kode_barang', '$nama_barang', '$kode_gudang')";

    if (mysqli_query($conn, $sql)) {
        return true;
    } else {
        return false;
    }
}

function ubah_barang($kode_barang, $nama_barang, $kode_gudang)
{
    global $conn;

    $kode_barang = mysqli_real_escape_string($conn, $kode_barang);
    $nama_barang = mysqli_real_escape_string($conn, $nama_barang);
    $kode_gudang = mysqli_real_escape_string($conn, $kode_gudang);

    // Query untuk mengubah data
    $sql = "UPDATE barang SET nama_barang='$nama_barang', kode_gudang='$kode_gudang' WHERE kode_barang='$kode_barang'";

    if (mysqli_query($conn, $sql)) {
        return true;
    } else {
        return false;
    }
}

function hapus_barang($kode_barang)
{
    global $conn;
    $kode_barang = mysqli_real_escape_string($conn, $kode_barang);

    // Query untuk menghapus data
    $sql = "DELETE FROM barang WHERE kode_barang='$kode_barang'";

    if (mysqli_query($conn, $sql)) {
        return true;
    } else {
        return false;
    }
}

function get_barang($kode_barang = null)
{
    global $conn;
    $sql = "SELECT barang.*, gudang.nama_gudang FROM barang INNER JOIN gudang ON barang.kode_gudang = gudang.kode_gudang";
    if ($kode_barang != null) {
        $kode_barang = mysqli_real_escape_string($conn, $kode_barang);

        // Query untuk mendapatkan data berdasarkan kode_barang
        $sql .= " WHERE kode_barang='$kode_barang'";
    }

    $result = mysqli_query($conn, $sql);

    // Mengubah hasil query menjadi array asosiatif
    $data = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $data[] = $row;
    }
    return $data;
}
