<?php

// Include file koneksi.php
include 'koneksi.php';
include 'gudang.php';

function get_barang_by_kode($kode_barang)
{
    global $conn;
    $sql = "SELECT * FROM barang WHERE kode_barang='$kode_barang'";
    $result = mysqli_query($conn, $sql);

    // Mengubah hasil query menjadi array asosiatif
    $data = mysqli_fetch_assoc($result);
    return $data;
}

function get_all_gudang()
{
    global $conn;
    $sql = "SELECT * FROM gudang";
    $result = mysqli_query($conn, $sql);

    // Membuat array kosong untuk menyimpan data gudang
    $data_gudang = array();

    // Looping untuk mengubah hasil query menjadi array asosiatif dan menyimpannya ke dalam $data_gudang
    while ($gudang = mysqli_fetch_assoc($result)) {
        $data_gudang[] = $gudang;
    }

    return $data_gudang;
}


$kode_barang = $_GET['kode_barang'];

$data_barang = get_barang_by_kode($kode_barang);

if (!$data_barang) {
    die('Barang tidak ditemukan');
}

// Jika tombol simpan ditekan
if (isset($_POST['simpan'])) {
    // Mendapatkan data dari form
    $nama_barang = $_POST['nama_barang'];
    $kode_gudang = $_POST['kode_gudang'];

    // Query untuk mengupdate data barang
    $query = "UPDATE barang SET nama_barang='$nama_barang', kode_gudang='$kode_gudang' WHERE kode_barang='$kode_barang'";

    // Menjalankan query
    $result = mysqli_query($conn, $query);

    // Jika query berhasil dijalankan
    if ($result) {
        header('Location: index.php');
    } else {
        echo 'Gagal mengupdate data barang';
    }
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>Edit Barang</title>
</head>

<body>
    <h1>Edit Barang</h1>

    <form method="POST" action="">
        <label>Nama Barang</label>
        <input type="text" name="nama_barang" value="<?php echo $data_barang['nama_barang']; ?>">

        <br>

        <label>Kode Gudang</label>
        <select name="kode_gudang">
            <?php
            // Mendapatkan data gudang

            $data_gudang = get_all_gudang(); // Looping untuk menampilkan opsi pada select
            foreach ($data_gudang as $gudang) {
                $selected = ($gudang['kode_gudang'] == $data_barang['kode_gudang']) ? 'selected' : '';
                echo "<option value='" . $gudang['kode_gudang'] . "' $selected>" . $gudang['nama_gudang'] . "</option>";
            }
            ?>
        </select>

        <br>

        <button type="submit" name="simpan">Simpan</button>
    </form>
</body>

</html>