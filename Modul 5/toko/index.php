<?php
// Include file koneksi.php
include 'koneksi.php';
include 'gudang.php';
include 'barang.php';

// Proses tambah data gudang
if (isset($_POST['tambah_gudang'])) {
    $kode_gudang = $_POST['kode_gudang'];
    $nama_gudang = $_POST['nama_gudang'];
    $lokasi = $_POST['lokasi'];

    tambah_gudang($kode_gudang, $nama_gudang, $lokasi);

    header('Location: index.php');
}

// Proses ubah data gudang
if (isset($_POST['ubah_gudang'])) {
    $kode_gudang = $_POST['kode_gudang'];
    $nama_gudang = $_POST['nama_gudang'];
    $lokasi = $_POST['lokasi'];

    ubah_gudang($kode_gudang, $nama_gudang, $lokasi);

    header('Location: index.php');
}

// Proses hapus data gudang
if (isset($_GET['hapus_gudang'])) {
    $kode_gudang = $_GET['hapus_gudang'];

    hapus_gudang($kode_gudang);

    header('Location: index.php');
}

// Proses tambah data barang
if (isset($_POST['tambah_barang'])) {
    $kode_barang = $_POST['kode_barang'];
    $nama_barang = $_POST['nama_barang'];
    $kode_gudang = $_POST['kode_gudang'];

    tambah_barang($kode_barang, $nama_barang, $kode_gudang);

    header('Location: index.php');
}

// Proses ubah data barang
if (isset($_POST['ubah_barang'])) {
    $kode_barang = $_POST['kode_barang'];
    $nama_barang = $_POST['nama_barang'];
    $kode_gudang = $_POST['kode_gudang'];

    ubah_barang($kode_barang, $nama_barang, $kode_gudang);

    header('Location: index.php');
}

// Proses hapus data barang
if (isset($_GET['hapus_barang'])) {
    $kode_barang = $_GET['hapus_barang'];

    hapus_barang($kode_barang);

    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>Aplikasi Toko</title>
</head>

<body>
    <h1>Aplikasi Toko</h1>

    <h2>Data Gudang</h2>

    <table border="1">
        <tr>
            <th>Kode Gudang</th>
            <th>Nama Gudang</th>
            <th>Lokasi</th>
            <th>Aksi</th>
        </tr>
        <?php
        // Mendapatkan data gudang
        $data_gudang = get_gudang();

        foreach ($data_gudang as $gudang) {
            echo '<tr>';
            echo '<td>' . $gudang['kode_gudang'] . '</td>';
            echo '<td>' . $gudang['nama_gudang'] . '</td>';
            echo '<td>' . $gudang['lokasi'] . '</td>';
            echo '<td><a href="edit_gudang.php?kode_gudang=' . $gudang['kode_gudang'] . '">Edit</a> | <a href="index.php?hapus_gudang=' . $gudang['kode_gudang'] . '">Hapus</a></td>';
            echo '</tr>';
        }
        ?>
    </table>
    <h2>Tambah Data Gudang</h2>

    <form method="POST" action="index.php">
        <label>Kode Gudang</label>
        <input type="text" name="kode_gudang">

        <br>

        <label>Nama Gudang</label>
        <input type="text" name="nama_gudang">

        <br>

        <label>Lokasi</label>
        <input type="text" name="lokasi">

        <br>

        <button type="submit" name="tambah_gudang">Tambah Gudang</button>
    </form>

    <h2>Data Barang</h2>

    <table border="1">
        <tr>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th>Kode Gudang</th>
            <th>Nama Gudang</th>
            <th>Aksi</th>
        </tr>
        <?php
        // Mendapatkan data barang
        $data_barang = get_barang();

        foreach ($data_barang as $barang) {
            echo '<tr>';
            echo '<td>' . $barang['kode_barang'] . '</td>';
            echo '<td>' . $barang['nama_barang'] . '</td>';
            echo '<td>' . $barang['kode_gudang'] . '</td>';
            echo '<td>' . $barang['nama_gudang'] . '</td>';
            echo '<td><a href="edit_barang.php?kode_barang=' . $barang['kode_barang'] . '">Edit</a> | <a href="index.php?hapus_barang=' . $barang['kode_barang'] . '">Hapus</a></td>';
            echo '</tr>';
        }
        ?>
    </table>
    <h2>Tambah Data Barang</h2>

    <form method="POST" action="index.php">
        <label>Kode Barang</label>
        <input type="text" name="kode_barang">

        <br>

        <label>Nama Barang</label>
        <input type="text" name="nama_barang">

        <br>

        <label>Kode Gudang</label>
        <select name="kode_gudang">
            <?php
            // Mendapatkan data gudang
            $data_gudang = get_gudang();

            foreach ($data_gudang as $gudang) {
                echo '<option value="' . $gudang['kode_gudang'] . '">' . $gudang['nama_gudang'] . ' (' . $gudang['lokasi'] . ')</option>';
            }
            ?>
        </select>

        <br>

        <button type="submit" name="tambah_barang">Tambah Barang</button>
    </form>
</body>

</html>