<?php

$host = "localhost"; // Host database
$user = "root"; // Username database
$pass = ""; // Password database
$db = "toko"; // Nama database

// Membuat koneksi ke database
$conn = mysqli_connect($host, $user, $pass, $db);

// Cek koneksi
if (!$conn) {
    die("Koneksi gagal: " . mysqli_connect_error());
}
