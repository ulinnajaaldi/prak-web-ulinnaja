<?php

include 'koneksi.php';

function tambah_gudang($kode_gudang, $nama_gudang, $lokasi)
{
    global $conn;

    $kode_gudang = mysqli_real_escape_string($conn, $kode_gudang);
    $nama_gudang = mysqli_real_escape_string($conn, $nama_gudang);
    $lokasi = mysqli_real_escape_string($conn, $lokasi);

    // Query untuk menambahkan data gudang
    $sql = "INSERT INTO gudang (kode_gudang, nama_gudang, lokasi) VALUES ('$kode_gudang', '$nama_gudang', '$lokasi')";

    if (mysqli_query($conn, $sql)) {
        return true;
    } else {
        return false;
    }
}

function ubah_gudang($kode_gudang, $nama_gudang, $lokasi)
{
    global $conn;

    $kode_gudang = mysqli_real_escape_string($conn, $kode_gudang);
    $nama_gudang = mysqli_real_escape_string($conn, $nama_gudang);
    $lokasi = mysqli_real_escape_string($conn, $lokasi);

    // Query untuk mengubah data gudang
    $sql = "UPDATE gudang SET nama_gudang='$nama_gudang', lokasi='$lokasi' WHERE kode_gudang='$kode_gudang'";

    if (mysqli_query($conn, $sql)) {
        return true;
    } else {
        return false;
    }
}

function hapus_gudang($kode_gudang)
{
    global $conn;
    $kode_gudang = mysqli_real_escape_string($conn, $kode_gudang);

    // Query untuk menghapus data gudang
    $sql = "DELETE FROM gudang WHERE kode_gudang='$kode_gudang'";

    if (mysqli_query($conn, $sql)) {
        return true;
    } else {
        return false;
    }
}

function get_gudang($kode_gudang = null)
{
    global $conn;

    // Query untuk mengambil semua data gudang
    $sql = "SELECT * FROM gudang";

    // Jika kode_gudang tidak null, tambahkan WHERE clause untuk memfilter berdasarkan kode_gudang
    if ($kode_gudang != null) {
        $kode_gudang = mysqli_real_escape_string($conn, $kode_gudang);
        $sql .= " WHERE kode_gudang='$kode_gudang'";
    }

    $result = mysqli_query($conn, $sql);

    // Konversi hasil query ke array asosiatif
    $data = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $data[] = $row;
    }

    return $data;
}
